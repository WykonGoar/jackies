﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProductDescriptionScript : MonoBehaviour {

    public GameObject LabelName;
    public GameObject LabelAmount;
    public GameObject LabelPrice;

    private Text tName;
    private Text tAmount;
    private Text tPrice;

    private ShopItem mShopItem;
    private LevelScript levelScript;

	// Use this for initialization
	void Start () {
        tName = LabelName.GetComponent<Text>();
        tAmount = LabelAmount.GetComponent<Text>();
        tPrice = LabelPrice.GetComponent<Text>();

        levelScript = LevelScript.CREATE();
	}
	
	// Update is called once per frame
	void Update () {
        if (levelScript.SelectedCartProductId != 0)
        {
            mShopItem = levelScript.GetShopItemByKey(levelScript.SelectedCartProductId);

            tName.text = mShopItem.Name;
            tAmount.text = "" + mShopItem.Amount;
            tPrice.text = mShopItem.GetTotalPrice().ToString("C");
        }
	}
    
    public void IncreaseAmount()
    {
        mShopItem.Amount = mShopItem.Amount + 1;
    }

    public void DecreaseAmount()
    {
        mShopItem.Amount--;
        if (mShopItem.Amount == 0)
        {
            gameObject.SetActive(false);
        }
    }

    public void DeleteProduct()
    {
        mShopItem.Amount = 0;
        gameObject.SetActive(false);
    }


}
