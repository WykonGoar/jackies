﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ShopItem
{
    private string mName;
    private double mPrice;
    private int mAmount;

    public ShopItem(string name, double price)
    {
        mName = name;
        mPrice = price;
        mAmount = 0;
    }

    public string Name { get { return mName; } }
    public double Price { get { return mPrice; } }

    public int Amount { get { return mAmount; } set { mAmount = value; } }

    public double GetTotalPrice()
    {
        return mPrice * mAmount;
    }
}
