﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TextboxScript : MonoBehaviour {

    public string name;
    public GameObject Wheel;
    //public GameObject WheelLabel;
    //public GameObject InputField;
    //public GameObject Label;

    public Text inputFieldText;
    public Text labelText;
    //private Text wheelLabelText;

	// Use this for initialization
	void Start () {
        //inputFieldText = InputField.GetComponent<Text>();
        //wheelLabelText = WheelLabel.GetComponent<Text>();
        //Text labelText = Label.GetComponent<Text>()
        //Label.GetComponent<Text>().text = name;
        labelText.text = name;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OpenInputWheel()
    {
        Wheel.SetActive(true);
        typingScript wheelScript = Wheel.GetComponent<typingScript>();

        wheelScript.OpenWheel(name, inputFieldText);
    }
}
