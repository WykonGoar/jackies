﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelScript {
    private static LevelScript mLevelScript;
    private Dictionary<int, ShopItem> shopItems = new Dictionary<int,ShopItem>();

    private GameObject selectedProductInCart;
    private int selectedCartProductId;
    private GameObject selectedProductItem;

    private LevelScript() 
    {
        CreateShopItems();
    }

    private void CreateShopItems()
    {
        ShopItem item1 = new ShopItem("Fridge", 250.00);
        ShopItem item2 = new ShopItem("Washing machine", 100.00);
        ShopItem item3 = new ShopItem("Laptop", 75.00);

        shopItems.Add(1, item1);
        shopItems.Add(2, item2);
        shopItems.Add(3, item3);
    }

    public static LevelScript CREATE()
    {
        if (mLevelScript == null)
            mLevelScript = new LevelScript();

        return mLevelScript;
    }

    public GameObject SelectedProductInCart { get { return selectedProductInCart; } set { selectedProductInCart = value; } }
    public int SelectedCartProductId { get { return selectedCartProductId; } set { selectedCartProductId = value; } }
    public GameObject SelectedProductItem { get { return selectedProductItem; } set { selectedProductItem = value; } }
    public ShopItem GetShopItemByKey(int key)
    {
        if (shopItems.ContainsKey(key))
        {
            return shopItems[key];
        }

        return null;
    }
    public double GetTotalAmount()
    {
        double total = 0.0;

        foreach(ShopItem item in shopItems.Values)
        {
            total += item.GetTotalPrice();
        }

        if (total > 0.0)
            total += 5;

        return total;
    }

    public void ResetShoppingCart()
    {
        foreach (ShopItem item in shopItems.Values)
        {
            item.Amount = 0;
        }
    }
}
