﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FormsScript : MonoBehaviour {

    public GameObject shoppingCart;
    public GameObject shelf;
    public GameObject signInCanvas;
    public GameObject createAccountCanvas;
    public GameObject orderDetailsCanvas;
    public GameObject paymentCanvas;
    public GameObject confirmationCanvas;
    public GameObject successfullCanvas;

    public GameObject labelToPayPayment;
    public GameObject labelPaymentMethodPayment;
    public GameObject labelToPayConfirm;
    public GameObject labelPaymentMethodConfirm;

    public GameObject register;
    public GameObject buyMaster;

    private Text textToPayPayment;
    private Text textPaymentMethodPayment;
    private Text textToPayConfirm;
    private Text textPaymentMethodConfirm;

    LevelScript levelScript;

    private int visibleForm = 1;
    private bool createAccount = false;
    private string paymentMethod = "iDeal";
    
	// Use this for initialization
	void Start () {
        textToPayPayment = labelToPayPayment.GetComponent<Text>();
        textPaymentMethodPayment = labelPaymentMethodPayment.GetComponent<Text>();
        textToPayConfirm = labelToPayConfirm.GetComponent<Text>();
        textPaymentMethodConfirm = labelPaymentMethodConfirm.GetComponent<Text>();

        levelScript = LevelScript.CREATE();
	}
	
	// Update is called once per frame
	void Update () {
        textToPayPayment.text = "You have to pay " + levelScript.GetTotalAmount().ToString("C");
        textPaymentMethodPayment.text = "Selected payment method is " + paymentMethod;

        textToPayConfirm.text = levelScript.GetTotalAmount().ToString("C");
        textPaymentMethodConfirm.text = paymentMethod;
	}

    public void ChoosePaymentMethod(string method)
    {
        paymentMethod = method;
    }

    public void ChangeAnimationState(int state)
    {
        Animator m_animRegister = register.GetComponent<Animator>();
        m_animRegister.SetInteger("State", state);

        Animator m_animBuyMaster = buyMaster.GetComponent<Animator>();
        m_animBuyMaster.SetInteger("State", state);
    }

    public void StartCheckout()
    {
        ChangeAnimationState(1);

        signInCanvas.SetActive(true);
    }

    public void EndCheckout(bool payed)
    {
        HideForms();
        shelf.SetActive(true);
        visibleForm = 1;
        //shoppingCart.SetActive(true);

        ChangeAnimationState(2);

        if (payed)
            levelScript.ResetShoppingCart();
    }

    public void SetCreateAccount(bool state)
    {
        createAccount = state;
    }

    public void BackOrderDetails()
    {
        if (createAccount)
            ChangeCheckoutState(2);
        else
            ChangeCheckoutState(1);
    }

    public void NextForm()
    {
        visibleForm++;

        if (visibleForm == 2 && !createAccount)
            visibleForm = 3;

        Debug.Log(visibleForm);
        ChangeCheckoutState(visibleForm);
    }

    public void PreviousForm()
    {
        visibleForm--;

        if (visibleForm == 2 && !createAccount)
            visibleForm = 1;

        ChangeCheckoutState(visibleForm);
    }

    private void ChangeCheckoutState(int state)
    {
        HideForms();

        switch (state)
        {
            case 1:
                signInCanvas.SetActive(true);
                break;
            case 2:
                createAccountCanvas.SetActive(true);
                break;
            case 3:
                orderDetailsCanvas.SetActive(true);
                break;
            case 4:
                paymentCanvas.SetActive(true);
                break;
            case 5:
                confirmationCanvas.SetActive(true);
                break;
            case 6:
                successfullCanvas.SetActive(true);
                break;
        }
    }

    private void HideForms()
    {
        signInCanvas.SetActive(false);
        createAccountCanvas.SetActive(false);
        orderDetailsCanvas.SetActive(false);
        paymentCanvas.SetActive(false);
        confirmationCanvas.SetActive(false);
        successfullCanvas.SetActive(false);
    }
}
