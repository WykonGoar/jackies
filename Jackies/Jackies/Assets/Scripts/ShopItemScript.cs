﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShopItemScript : MonoBehaviour {

    public int shopItemId;
    public GameObject CanvasItem;
    public GameObject LabelName;
    public GameObject LabelPrice;
    public GameObject CanvasProductDescription;
    public GameObject Fonnel;

    private Text tName;
    private Text tPrice;

    private LevelScript levelScript;

    private float totalTime;
    private bool countTimeForAddingProduct = false;

    private ShopItem mShopItem;
    private bool isSelected = false;

	// Use this for initialization
	void Start () {
        tName = LabelName.GetComponent<Text>();
        tPrice = LabelPrice.GetComponent<Text>();

        levelScript = LevelScript.CREATE();
        mShopItem = levelScript.GetShopItemByKey(shopItemId);

        tName.text = mShopItem.Name;
        tPrice.text = mShopItem.Price.ToString("C");
	}
	
	// Update is called once per frame
    void Update()
    {
        if(countTimeForAddingProduct)
        {
            totalTime += Time.deltaTime;
            if(totalTime > 1.2)
            {
                mShopItem.Amount++;
                countTimeForAddingProduct = false;
                Fonnel.SetActive(false);
                ChangeAnimationState(1);
            }
        }
    }

    public void ChangeAnimationState(int state)
    {
        Animator m_anim = this.GetComponent<Animator>();

        if (state == 3 || (state == 1 && !countTimeForAddingProduct) || (state == 2 && !isSelected))
            m_anim.SetInteger("State", state);
    }

    public void AddProductToCart()
    {
        Fonnel.SetActive(true);
        ChangeAnimationState(3);

        totalTime = 0;
        countTimeForAddingProduct = true;        
    }

    public void Select()
    {      
        if (isSelected)
        {
            Deselect();           
        }
        else
        {
            isSelected = true;
            CanvasItem.SetActive(true);

            ChangeAnimationState(1); 

            DeselectLastProducts();
            levelScript.SelectedProductItem = gameObject;
        }
    }

    public void Deselect()
    {
        isSelected = false;
        ChangeAnimationState(2);

        CanvasItem.SetActive(false);
    }

    public void DeselectLastProducts()
    {
        GameObject lastSelectedProductInCart = levelScript.SelectedProductInCart;

        if (lastSelectedProductInCart != null)
            lastSelectedProductInCart.GetComponent<ShoppingItemInCartScript>().Deselect();

        CanvasProductDescription.SetActive(false);
        
        GameObject lastSelectedProductItem = levelScript.SelectedProductItem;

        if (lastSelectedProductItem != null && lastSelectedProductItem != gameObject)
            lastSelectedProductItem.GetComponent<ShopItemScript>().Deselect();
    }
}
