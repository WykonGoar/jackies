﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SignScript : MonoBehaviour {

    public GameObject LabelTotal;
    public GameObject LabelShipping;
    public GameObject Forms;
    public GameObject Shelf;
    public GameObject ProductDescriptionCanvas;
    public GameObject Shoppingcart;

    private Text tTotal;
    private Text tShipping;

    private LevelScript levelScript;
    private ShopItem mShopItem;

	// Use this for initialization
	void Start () {
        tTotal = LabelTotal.GetComponent<Text>();
        tShipping = LabelShipping.GetComponent<Text>();

        levelScript = LevelScript.CREATE();
	}
	
	// Update is called once per frame
	void Update () {
        double total = levelScript.GetTotalAmount();
        tTotal.text = "Total " + total.ToString("C");

        double shippingCost = 0.0;

        if (total != 0.0)
            shippingCost = 5.0;
        
        tShipping.text = "Shipping " + shippingCost.ToString("C");
	}

    public void StartCheckout()
    {
        if (levelScript.GetTotalAmount() > 0.0)
        {
            Debug.Log("Start checkout");
            ProductDescriptionCanvas.SetActive(false);
            
            Forms.GetComponent<FormsScript>().StartCheckout();
            //Shelf.SetActive(false);
            //Shoppingcart.SetActive(false);
        }
    }
}
