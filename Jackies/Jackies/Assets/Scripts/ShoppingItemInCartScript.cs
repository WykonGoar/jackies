﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class ShoppingItemInCartScript : MonoBehaviour {
    public GameObject desciptionCanvas;
    public int ShopItemId;
    public GameObject model;

    private LevelScript levelScript;
    private ShopItem mShopItem;

    private bool isSelected = false;

	// Use this for initialization
    void Start()
    {
        levelScript = LevelScript.CREATE();
    }
	
	// Update is called once per frame
	void Update () {
        mShopItem = levelScript.GetShopItemByKey(ShopItemId);

        if (mShopItem != null)
        {
            if (mShopItem.Amount > 0)
                model.SetActive(true);

            else
                model.SetActive(false);
        }
	}
    
    public void ChangeAnimationState(int state)
    {
        Animator m_anim = this.GetComponent<Animator>();       
 
        if(state != 2 || (state == 2 && !isSelected))
        m_anim.SetInteger("State", state);
    }

    public void Select()
    {
        if (isSelected)
        {
            Deselect();
            desciptionCanvas.SetActive(false);
            levelScript.SelectedCartProductId = 0;
        }
        else
        {
            isSelected = true;
            desciptionCanvas.SetActive(true);
            DeselectLastProducts();

            ChangeAnimationState(1); 
            
            levelScript.SelectedProductInCart = gameObject;
            levelScript.SelectedCartProductId = ShopItemId;
        }
    }

    public void Deselect()
    {
        isSelected = false;
        ChangeAnimationState(2);            
    }

    public void DeselectLastProducts()
    {
        GameObject lastSelectedProductInCart = levelScript.SelectedProductInCart;

        if (lastSelectedProductInCart != null && lastSelectedProductInCart != gameObject)
            lastSelectedProductInCart.GetComponent<ShoppingItemInCartScript>().Deselect();

        GameObject lastSelectedProductItem = levelScript.SelectedProductItem;

        if (lastSelectedProductItem != null)
            lastSelectedProductItem.GetComponent<ShopItemScript>().Deselect();
    }
}
