﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class typingScript : MonoBehaviour {
    public string word = ""; //this is the string itself
    public bool numbers = false; //decides if it types numbers or letters. Use to show what buttons do
    public bool caps = false; //decides if letters are big or not. Use to show what buttons do
    public int counter = -1; //decides which letter was last typed, or selected with back/forward buttons
    public Text currentText;
    public Text btn1;
    public Text btn2;
    public Text btn3;
    public Text btn4;
    public Text btn5;
    public Text btn6;
    public Text btn7;
    public Text btn8;
    public Text btn9;
    public Text btn10;
    public Text btn11;
    public Text btn12;
    public Text btn13;
    public Text btn14;
    public Text btn15;
    public Text btn16;
    public Text btn17;
    public Text btn18;
    public Text btn19;
    public Text btn20;
    public Text btn21;
    public Text btn22;
    public Text btn23;
    public Text btn24;
    public Text btn25;
    public Text btn26;
    public Text numTxt;
    public Text currentTitle;
    // Use this for initialization

    private Text currentTextbox;

    void Start () {
	}

    void Update()
    {
        currentText.text = word;
    }
    //Every button has a function that needs to be called by the program in order to use it, the buttons function as they
    //seem like they would, caps turning on caps lock, back/forward changes position, backspace removes letter in position
    //cancel/confirm cancels or confirms selection, numbers changes to numbers instead of letters.
	public void btn1press()
    {
        if (numbers)
            word += "1";
        else
            if (caps)
            word += "A";
        else
            word += "a";
        counter++;
    }

    public void btn2press()
    {
        if (numbers)
            word += "2";
        else
            if (caps)
            word += "B";
        else
            word += "b";
        counter++;
    }

    public void btn3press()
    {
        if (numbers)
            word += "3";
        else
            if (caps)
            word += "C";
        else
            word += "c";
        counter++;
    }

    public void btn4press()
    {
        if (numbers)
            word += "4";
        else
            if (caps)
            word += "D";
        else
            word += "d";
        counter++;
    }

    public void btn5press()
    {
        if (numbers)
            word += "5";
        else
            if (caps)
            word += "E";
        else
            word += "e";
        counter++;
    }

    public void btn6press()
    {
        if (numbers)
            word += "6";
        else
            if (caps)
            word += "F";
        else
            word += "f";
        counter++;
    }

    public void btn7press()
    {
        if (numbers)
            word += "7";
        else
            if (caps)
            word += "G";
        else
            word += "g";
        counter++;
    }

    public void btn8press()
    {
        if (numbers)
            word += "8";
        else
            if (caps)
            word += "H";
        else
            word += "h";
        counter++;
    }

    public void btn9press()
    {
        if (numbers)
            word += "9";
        else
            if (caps)
            word += "I";
        else
            word += "i";
        counter++;
    }

    public void btn10press()
    {
        if (numbers)
            word += "0";
        else
            if (caps)
            word += "J";
        else
            word += "j";
        counter++;
    }

    public void btn11press()
    {
        if (numbers)
            word += "-";
        else
            if (caps)
            word += "K";
        else
            word += "k";
        counter++;
    }

    public void btn12press()
    {
        if (numbers)
            word += "_";
        else
            if (caps)
            word += "L";
        else
            word += "l";
        counter++;
    }

    public void btn13press()
    {
        if (numbers)
            word += "'";
        else
            if (caps)
            word += "M";
        else
            word += "m";
        counter++;
    }

    public void btn14press()
    {
        if (numbers)
            word += "`";
        else
            if (caps)
            word += "N";
        else
            word += "n";
        counter++;
    }

    public void btn15press()
    {
        if (numbers)
            word += "~";
        else
            if (caps)
            word += "O";
        else
            word += "o";
        counter++;
    }

    public void btn16press()
    {
        if (numbers)
            word += "^";
        else
            if (caps)
            word += "P";
        else
            word += "p";
        counter++;
    }

    public void btn17press()
    {
        if (numbers)
            word += "@";
        else
            if (caps)
            word += "Q";
        else
            word += "q";
        counter++;
    }

    public void btn18press()
    {
        if (numbers)
            word += ":";
        else
            if (caps)
            word += "R";
        else
            word += "r";
        counter++;
    }

    public void btn19press()
    {
        if (numbers)
            word += ";";
        else
            if (caps)
            word += "S";
        else
            word += "s";
        counter++;
    }

    public void btn20press()
    {
        if (numbers)
            word += "#";
        else
            if (caps)
            word += "T";
        else
            word += "t";
        counter++;
    }

    public void btn21press()
    {
        if (numbers)
            word += "(";
        else
            if (caps)
            word += "U";
        else
            word += "u";
        counter++;
    }

    public void btn22press()
    {
        if (numbers)
            word += ")";
        else
            if (caps)
            word += "V";
        else
            word += "v";
        counter++;
    }

    public void btn23press()
    {
        if (numbers)
            word += "/";
        else
            if (caps)
            word += "W";
        else
            word += "w";
        counter++;
    }

    public void btn24press()
    {
        if (numbers)
            word += "+";
        else
            if (caps)
            word += "X";
        else
            word += "x";
        counter++;
    }

    public void btn25press()
    {
        if (numbers)
            word += ".";
        else
            if (caps)
            word += "Y";
        else
            word += "y";
        counter++;
    }

    public void btn26press()
    {
        if (numbers)
            word += ",";
        else
            if (caps)
            word += "Z";
        else
            word += "z";
        counter++;
    }

    public void btnBck()
    {
        if (counter > 0)
        counter--;
    }

    public void btnFwd()
    {
        if (counter <= word.Length)
            counter++;
    }

    public void btnBsp()
    {
        word.Remove(counter, 1);
        //word.Remove(word.Length - 1); removes last letter only
    }

    public void btnNum()
    {
        if (numbers)
        {
            if (caps)
                uCase();
            else
                lCase();
            numbers = false;
            numTxt.text = "123...";
        }

        else
        {
            numbers = true;
            numerical();
            if (caps)
                numTxt.text = "ABC...";
            else
                numTxt.text = "abc...";
        }
            
    }
    public void btnCpl()
    {
        if (!numbers)
            {
            if (caps)
            {
                caps = false;
                lCase();
            }

            else
            {
                caps = true;
                uCase();
            }
                
            }
    }

    public void OpenWheel(string name, Text textbox)
    {
        currentTitle.text = name;
        currentTextbox = textbox;

        if (textbox.text != "Enter text...")
            word = textbox.text;
        else
            word = "";
    }

    public void btnCfm()
        {
        //This needs to input the "word" string into the right form
        //TODO add text to form
        counter = -1;
        currentTextbox.text = word;
        gameObject.SetActive(false);
    }
    public void btnCcl()
    {
        //this button cancels the current text and goes back to form
        counter = -1;
        gameObject.SetActive(false);
    }

    void lCase()
    {
        btn1.text = "a";
        btn2.text = "b";
        btn3.text = "c";
        btn4.text = "d";
        btn5.text = "e";
        btn6.text = "f";
        btn7.text = "g";
        btn8.text = "h";
        btn9.text = "i";
        btn10.text = "j";
        btn11.text = "k";
        btn12.text = "l";
        btn13.text = "m";
        btn14.text = "n";
        btn15.text = "o";
        btn16.text = "p";
        btn17.text = "q";
        btn18.text = "r";
        btn19.text = "s";
        btn20.text = "t";
        btn21.text = "u";
        btn22.text = "v";
        btn23.text = "w";
        btn24.text = "x";
        btn25.text = "y";
        btn26.text = "z";
    }

    void uCase()
    {
        btn1.text = "A";
        btn2.text = "B";
        btn3.text = "C";
        btn4.text = "D";
        btn5.text = "E";
        btn6.text = "F";
        btn7.text = "G";
        btn8.text = "H";
        btn9.text = "I";
        btn10.text = "J";
        btn11.text = "K";
        btn12.text = "L";
        btn13.text = "M";
        btn14.text = "N";
        btn15.text = "O";
        btn16.text = "P";
        btn17.text = "Q";
        btn18.text = "R";
        btn19.text = "S";
        btn20.text = "T";
        btn21.text = "U";
        btn22.text = "V";
        btn23.text = "W";
        btn24.text = "X";
        btn25.text = "Y";
        btn26.text = "Z";
    }

    void numerical()
    {
        btn1.text = "1";
        btn2.text = "2";
        btn3.text = "3";
        btn4.text = "4";
        btn5.text = "5";
        btn6.text = "6";
        btn7.text = "7";
        btn8.text = "8";
        btn9.text = "9";
        btn10.text = "0";
        btn11.text = "-";
        btn12.text = "_";
        btn13.text = "'";
        btn14.text = "`";
        btn15.text = "~";
        btn16.text = "^";
        btn17.text = "@";
        btn18.text = ":";
        btn19.text = ";";
        btn20.text = "#";
        btn21.text = "(";
        btn22.text = ")";
        btn23.text = "/";
        btn24.text = "+";
        btn25.text = ".";
        btn26.text = ",";
    }
}
